#include <stdio.h>
#include <limits.h>
#include <math.h>

long maxlong(void){
	return LONG_MAX;
}

double upper_bound(long n){
	double a;
	while ( n >= 6){
		a = pow((n*0.5), n);
		return a;
	}
	while (n >= 0 && n < 6){
		a = 719;
		return a;
	}
}

long factorial(long n){
	long x = 1;
	long f = 1;
	if(n < 0){
		f = -2;
	}
	else if (upper_bound(n) > maxlong()){
		return -1;
	}
	else {
		while(x <= n && x >= 0){
		f = f * x;
		x = x+1;
	
	}

	}
	
	return f;
	
}



int main(void){
	long i; 

	printf("maxlong()=%ld\n", maxlong());

	for (i=0; i<10; i++){
		printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
	} 
	return 0; 
}